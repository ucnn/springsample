package com.sample.exception;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;

/**
 * Created by lmh on 2016. 9. 9..
 */
public class InvalidParameterException extends Exception {
    private List<ObjectError> errorList;

    public InvalidParameterException(BindingResult result) {
        this.errorList = result.getAllErrors();
    }

    public List<ObjectError> getErrorList() {
        return errorList;
    }
}
