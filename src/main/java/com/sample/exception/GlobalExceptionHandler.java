package com.sample.exception;

import com.sample.constants.ErrorCode;
import com.sample.model.response.DefaultErrorData;
import com.sample.model.response.InvalidParameterErrorData;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lmh on 2016. 9. 9..
 */
@ControllerAdvice(annotations = {RestController.class})
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public DefaultErrorData defaultExceptionHandling(Exception e) {
        DefaultErrorData<String> errorData = new DefaultErrorData<>(ErrorCode.ERROR_COMMON);
        errorData.setMessage(e.getMessage());
        return errorData;
    }

    @ExceptionHandler(value = InvalidParameterException.class)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public InvalidParameterErrorData invalidInputParameterException(InvalidParameterException e) {
        InvalidParameterErrorData errorData = new InvalidParameterErrorData(ErrorCode.ERROR_INVALID_PARAMETER);
        errorData.setFieldErrorList(e.getErrorList());
        return errorData;
    }
}
