package com.sample.service;

import com.sample.model.sample.SampleData;

import java.util.List;

/**
 * Created by lmh on 2016. 9. 1..
 */
public interface SampleService {

    SampleData getSampleData(int sampleIdx);

    List<SampleData> getSampleDataList(List<Integer> sampleIdxList);

    SampleData insertSample(String sampleString, int sampleInt, double SampleDouble);

    void deleteSample(int sampleIdx);
}
