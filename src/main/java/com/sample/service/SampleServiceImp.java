package com.sample.service;

import com.sample.model.sample.SampleData;
import com.sample.mapper.SampleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lmh on 2016. 9. 1..
 */
@Service("SampleService")
public class SampleServiceImp implements SampleService {

    @Autowired
    private SampleMapper sampleMapper;

    @Override
    public SampleData getSampleData(int sampleIdx) {
        return sampleMapper.getSampleData(sampleIdx);
    }

    @Override
    public List<SampleData> getSampleDataList(List<Integer> sampleIdxList) {

        List<SampleData> result = new ArrayList<>();

        for(int idx : sampleIdxList) {
            SampleData data = sampleMapper.getSampleData(idx);
            if(data != null) {
                result.add(data);
            }
        }

        return result;
    }

    @Override
    public SampleData insertSample(String sampleString, int sampleInt, double sampleDouble) {
        SampleData sampleData = new SampleData();
        sampleData.setSampleString(sampleString);
        sampleData.setSampleInt(sampleInt);
        sampleData.setSampleDouble(sampleDouble);
        sampleMapper.insertSampleData(sampleData);
        return sampleData;
    }

    @Override
    public void deleteSample(int sampleIdx) {
        sampleMapper.deleteSampleData(sampleIdx);
    }
}
