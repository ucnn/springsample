package com.sample.util;

import javax.servlet.http.HttpServletRequest;

public class RequestUtils {
    public static String getBaseURL(HttpServletRequest request) {
        return String.format("%s://%s:%s%s", request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath());
    }
}
