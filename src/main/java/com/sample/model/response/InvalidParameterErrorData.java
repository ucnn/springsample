package com.sample.model.response;

import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lmh on 2016. 9. 9..
 */
public class InvalidParameterErrorData extends ErrorData<List<InvalidParameterErrorData.ErrorInfo>> {
    public InvalidParameterErrorData(int code) {
        super(code);
    }

    public void setFieldErrorList(List<ObjectError> objectErrorList) {
        List<ErrorInfo> errorList = new ArrayList<>();

        if(objectErrorList != null && !objectErrorList.isEmpty()){
            errorList.addAll(objectErrorList.stream().map(error -> new ErrorInfo(((org.springframework.validation.FieldError) error).getField(), error.getDefaultMessage())).collect(Collectors.toList()));
        }
        setMessage(errorList);
    }

    public class ErrorInfo {
        private String field;
        private String message;

        public ErrorInfo(String field, String message) {
            this.field   = field;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public void setField(String field) {
            this.field = field;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
