package com.sample.model.response;

public class SuccessResponse<T> extends ResponseData {
    private T data;

    public SuccessResponse() {}

    public SuccessResponse(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}