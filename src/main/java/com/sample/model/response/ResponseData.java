package com.sample.model.response;

import com.sample.constants.DataCode;

public class ResponseData {
    private int code;

    public ResponseData() {
        this.code = DataCode.CODE_SUCCESS;
    }

    public ResponseData(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
