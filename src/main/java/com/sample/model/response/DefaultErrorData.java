package com.sample.model.response;

/**
 * Created by lmh on 2016. 9. 9..
 */
public class DefaultErrorData<T> extends ErrorData<T> {

    public DefaultErrorData(int code) {
        super(code);
    }
}
