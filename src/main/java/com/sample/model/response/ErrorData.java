package com.sample.model.response;

public abstract class ErrorData<T> extends ResponseData {
    private T message;

    public ErrorData(int code) {
        super(code);
    }

    public T getMessage() {
        return message;
    }

    public void setMessage(T message) {
        this.message = message;
    }
}


