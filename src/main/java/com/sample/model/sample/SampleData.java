package com.sample.model.sample;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.sample.serializer.DateYmdSerializer;

import java.util.Date;

/**
 * Created by lmh on 2016. 9. 1..
 */
public class SampleData {

    @SerializedName("idx")
    private int sampleIdx;

    @SerializedName("string")
    private String sampleString;

    @SerializedName("int")
    private int sampleInt;

    @SerializedName("double")
    private double sampleDouble;

    @SerializedName("date")
    @JsonAdapter(DateYmdSerializer.class)
    private Date sampleDate;

    public int getSampleIdx() {
        return sampleIdx;
    }

    public void setSampleIdx(int sampleIdx) {
        this.sampleIdx = sampleIdx;
    }

    public String getSampleString() {
        return sampleString;
    }

    public void setSampleString(String sampleString) {
        this.sampleString = sampleString;
    }

    public int getSampleInt() {
        return sampleInt;
    }

    public void setSampleInt(int sampleInt) {
        this.sampleInt = sampleInt;
    }

    public double getSampleDouble() {
        return sampleDouble;
    }

    public void setSampleDouble(double sampleDouble) {
        this.sampleDouble = sampleDouble;
    }

    public Date getSampleDate() {
        return sampleDate;
    }

    public void setSampleDate(Date sampleDate) {
        this.sampleDate = sampleDate;
    }
}
