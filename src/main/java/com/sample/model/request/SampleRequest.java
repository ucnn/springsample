package com.sample.model.request;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Range;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Size;

/**
 * Created by lmh on 2016. 9. 9..
 */
public class SampleRequest {

    @Email
    @Size(min=3, max = 20)
    private String email;

    @URL
    private String url;

    @Range(min = 0, max=100)
    private int range;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }
}
