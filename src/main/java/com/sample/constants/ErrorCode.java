package com.sample.constants;

/**
 * Created by lmh on 2016. 9. 9..
 */
public interface ErrorCode {

    int ERROR_INVALID_PARAMETER = 1000;

    int ERROR_COMMON = 9999;

}
