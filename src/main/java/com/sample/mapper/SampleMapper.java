package com.sample.mapper;

import com.sample.model.sample.SampleData;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.javassist.tools.reflect.Sample;

/**
 * Created by lmh on 2016. 9. 1..
 */
public interface SampleMapper {

    SampleData getSampleData(@Param("sampleIdx") int sampleIdx);

    void insertSampleData(SampleData insertSampleData);

    void deleteSampleData(@Param("sampleIdx") int sampleIdx);
}
