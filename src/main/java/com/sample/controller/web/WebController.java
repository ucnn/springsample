package com.sample.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by lmh on 2016. 9. 1..
 */
@Controller
public class WebController {

    @RequestMapping(value={"/main"}, method= RequestMethod.GET)
    public String main(ModelMap model) {
        model.addAttribute("name", "Spring Sample");
        return "main";
    }
}
