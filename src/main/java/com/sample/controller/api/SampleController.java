package com.sample.controller.api;

import com.sample.exception.InvalidParameterException;
import com.sample.model.request.SampleRequest;
import com.sample.model.response.SuccessResponse;
import com.sample.model.sample.SampleData;
import com.sample.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by lmh on 2016. 8. 23..
 */
@RestController
@RequestMapping({"v1/sample", "v2/sample"})
public class SampleController {

    @Autowired
    private SampleService sampleService;

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public SuccessResponse<SampleData> sampleRequest(@PathVariable(value = "id") int id){

        SampleData data = sampleService.getSampleData(id);

        return new SuccessResponse<>(data);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public SuccessResponse<SampleData> insert(@RequestParam("sample_string")    String  sampleString,
                                              @RequestParam("sample_int")       int     sampleInt,
                                              @RequestParam("sample_double")    double  sampleDouble){

        SampleData insertedSampleData = sampleService.insertSample(sampleString, sampleInt, sampleDouble);

        return new SuccessResponse<>(insertedSampleData);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public SuccessResponse<HashMap> delete(@RequestParam("sample_idx") int sampleIdx) {
        sampleService.deleteSample(sampleIdx);

        HashMap resData = new LinkedHashMap();
        resData.put("id", sampleIdx);

        return new SuccessResponse<>(resData);
    }

    @RequestMapping(value = "/valid", method = RequestMethod.POST)
    public SuccessResponse<SampleRequest> delete(@Valid SampleRequest request, BindingResult result) throws InvalidParameterException {

        //Validation 체크의 에러가 있으면 에러를 throw 한다
        if(result.hasErrors()) {
            throw new InvalidParameterException(result);
        }

        return new SuccessResponse<>(request);
    }
}
