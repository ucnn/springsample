# README #

Spring Base Sample Project


## Environment Variable##

```
#!c

-DDB_URL_LOG="jdbc:log4jdbc:mysql://localhost:3306/log"
-DDB_URL_APP="jdbc:mysql://localhost:3306/wmp"
-DDB_USERNAME="id"
-DDB_PASSWORD="password"

```

## Sample Table Schema ##


```
#!Sql

CREATE TABLE `sample` (
  `sampleIdx` int(11) NOT NULL AUTO_INCREMENT,
  `sampleString` varchar(45) DEFAULT NULL,
  `sampleInt` int(11) DEFAULT NULL,
  `sampleDouble` double DEFAULT NULL,
  `sampleDate` datetime DEFAULT NULL,
  PRIMARY KEY (`sampleIdx`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

```